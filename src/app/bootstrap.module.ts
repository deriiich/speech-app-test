import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CarouselModule } from "ngx-bootstrap/carousel";
import { CollapseModule } from "ngx-bootstrap/collapse";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faCalendar, faSearch } from "@fortawesome/free-solid-svg-icons";
import {
  faFacebook,
  faInstagram,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import { TooltipModule } from "ngx-bootstrap/tooltip";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BsDropdownModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    CarouselModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  exports: [
    BsDropdownModule,
    BsDatepickerModule,
    CollapseModule,
    ModalModule,
    CarouselModule,
    TooltipModule,
  ],
})
export class BootstrapModule {
  constructor() {
    library.add(faSearch, faCalendar, faTwitter, faFacebook, faInstagram);
  }
}
