import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MySpeechesComponent } from "./components/my-speeches/my-speeches.component";
import { SaveSpeechComponent } from "./common-components/save-speech/save-speech.component";
import { SearchSpeechComponent } from "./components/search-speech/search-speech.component";
import { CreateSpeechComponent } from "./components/create-speech/create-speech.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/my-speeches",
    pathMatch: "full",
  },
  {
    path: "my-speeches",
    component: MySpeechesComponent,
  },
  {
    path: "create-speech",
    component: CreateSpeechComponent,
  },
  {
    path: "search-speech",
    component: SearchSpeechComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: "legacy" })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
