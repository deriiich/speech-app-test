import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BootstrapModule } from "./bootstrap.module";
import { SharedModule } from "./common-components/shared.module";
import { CreateSpeechComponent } from "./components/create-speech/create-speech.component";
import { MySpeechesModule } from "./components/my-speeches/my-speeches.module";
import { SearchSpeechComponent } from "./components/search-speech/search-speech.component";
import { HeaderComponent } from "./components/header/header.component";
import { MainViewModule } from "./main-view/main-view.module";
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchSpeechComponent,
    CreateSpeechComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    BootstrapModule,
    SharedModule,
    MainViewModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
