import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Component } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { MainViewConfig } from "./core/models/mainview-config";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title;
  speechChosen;
  url;
  shouldShowList;
  config: MainViewConfig;

  constructor(private router: Router) {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.url = e.url;

        if (e.url.includes("my-speeches")) {
          this.title = "My Speeches";
          this.config = {
            showSpeechList: true,
            showSearch: false,
          };
        } else if (e.url.includes("create-speech")) {
          this.title = "Create Speeches";
          this.config = {
            showSpeechList: false,
            showSearch: false,
          };
        } else if (e.url.includes("search-speech")) {
          this.title = "Search Speeches";
          this.config = {
            showSpeechList: false,
            showSearch: true,
          };
        } else {
          this.title = "Speech App";
        }
      }
    });
  }

  getChosen(event) {
    if (this.url === "/my-speeches") {
      this.speechChosen = event;
    } else {
      this.speechChosen = null;
    }
  }

  showList(shoudShowList: boolean) {
    this.shouldShowList = shoudShowList;
  }
}
