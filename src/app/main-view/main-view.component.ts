import { Component, Input, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { MainViewConfig } from "../core/models/mainview-config";
import { SpeechViewConfig } from "../core/models/speechView.config";

@Component({
  selector: "app-main-view",
  templateUrl: "./main-view.component.html",
  styleUrls: ["./main-view.component.scss"],
})
export class MainViewComponent implements OnInit {
  @Input("mainViewConfig") set mainViewConfig(config: MainViewConfig) {
    if (config) {
      this.showSearch = config.showSearch ? config.showSearch : false;
      this.showList = config.showSpeechList ? config.showSpeechList : false;
    }
  }

  speechViewConfig: SpeechViewConfig;

  selectedSpeech;
  speechList;
  showList;
  showSearch;
  constructor(private router: Router) {}

  ngOnInit(): void {
    this.router.events.subscribe((e) => {
      this.showList = false;
      this.speechViewConfig = {
        isSearching: false,
      };

      if (e instanceof NavigationEnd) {
        console.log(e.url === "/my-speeches");
        this.showList = true;
        this.selectedSpeech = null;
      }
    });
  }

  getSearchQuery(query) {
    if (query.length > 0) {
      this.showList = true;
      this.speechViewConfig = {
        isSearching: true,
      };
    } else {
      this.showList = false;
      this.speechViewConfig = {
        isSearching: false,
      };
    }
  }

  setSelectedSpeech(speechObj) {
    this.selectedSpeech = speechObj;
  }
}
