import { Injectable } from "@angular/core";
import { SpeechModel } from "../models/speech";
import { BehaviorSubject, Observable, of } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SpeechService {
  speeches: BehaviorSubject<SpeechModel[]> = new BehaviorSubject<SpeechModel[]>(
    null
  );
  showList: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  speechCache;
  filteredResult = [];

  speechesArray = [
    {
      author: "Derich",
      date: "Fri Jul 05 2019 00:00:00 GMT+0800 (Philippine Standard Time)",
      keywords: ["Derich1", "test1"],
      speechMessage:
        "Speech 1 As a kid, I spent my summers with my grandparents on their ranch in Texas. I helped fix windmills, vaccinate cattle, and do other chores. We also watched soap operas every afternoon, especially “Days of our Lives.” My grandparents belonged to a Caravan Club, a group of Airstream trailer owners who travel together around the U.S. and Canada. And every few summers, we’d join the caravan. We’d hitch up the Airstream trailer to my grandfather’s car, and off we’d go, in a line with 300 other Airstream adventurers. I loved and worshipped my grandparents and I really looked forward to these trips. On one particular trip, I was about 10 years old. I was rolling around in the big bench seat in the back of the car. My grandfather was driving. And my grandmother had the passenger seat. She smoked throughout these trips, and I hated the smell. At that age, I’d take any excuse to make estimates and do minor arithmetic. I’d calculate our gas mileage — figure out useless statistics on things like grocery spending. I’d been hearing an ad campaign about smoking. I can’t remember the details, but basically the ad said, every puff of a cigarette takes some number of minutes off of your life: I think it might have been two minutes per puff. At any rate, I decided to do the math for my grandmother. I estimated the number of cigarettes per days, estimated the number of puffs per cigarette and so on. When I was satisfied that I’d come up with a reasonable number, I poked my head into the front of the car, tapped my grandmother on the shoulder, and proudly proclaimed, At two minutes per puff, you’ve taken nine years off your life!",
    },
    {
      author: "Derich1",
      date: "Fri Jul 05 2019 00:00:00 GMT+0800 (Philippine Standard Time)",
      keywords: ["Derich2", "test2"],
      speechMessage:
        "Speech 2 At that age, I’d take any excuse to make estimates and do minor arithmetic. I’d calculate our gas mileage — figure out useless statistics on things like grocery spending. I’d been hearing an ad campaign about smoking. I can’t remember the details, but basically the ad said, every puff of a cigarette takes some number of minutes off of your life: I think it might have been two minutes per puff. At any rate, I decided to do the math for my grandmother. I estimated the number of cigarettes per days, estimated the number of puffs per cigarette and so on. When I was satisfied that I’d come up with a reasonable number, I poked my head into the front of the car, tapped my grandmother on the shoulder, and proudly proclaimed, “At two minutes per puff, you’ve taken nine years off your life!”",
    },
    {
      author: "Derich2",
      date: "Fri Jul 05 2019 00:00:00 GMT+0800 (Philippine Standard Time)",
      keywords: ["Derich3", "test3"],
      speechMessage:
        "Speech 3 I have a vivid memory of what happened, and it was not what I expected. I expected to be applauded for my cleverness and arithmetic skills. “Jeff, you’re so smart. You had to have made some tricky estimates, figure out the number of minutes in a year and do some division.” That’s not what happened. Instead, my grandmother burst into tears. I sat in the backseat and did not know what to do. While my grandmother sat crying, my grandfather, who had been driving in silence, pulled over onto the shoulder of the highway. He got out of the car and came around and opened my door and waited for me to follow. Was I in trouble? My grandfather was a highly intelligent, quiet man. He had never said a harsh word to me, and maybe this was to be the first time? Or maybe he would ask that I get back in the car and apologize to my grandmother. I had no experience in this realm with my grandparents and no way to gauge what the consequences might be. We stopped beside the trailer. My grandfather looked at me, and after a bit of silence, he gently and calmly said, “Jeff, one day you’ll understand that it’s harder to be kind than clever.”",
    },
    {
      author: "Derich3",
      date: "Fri Jul 05 2019 00:00:00 GMT+0800 (Philippine Standard Time)",
      keywords: ["Derich4", "test4"],
      speechMessage:
        "Speech 4 What I want to talk to you about today is the difference between gifts and choices. Cleverness is a gift, kindness is a choice. Gifts are easy — they’re given after all. Choices can be hard. You can seduce yourself with your gifts if you’re not careful, and if you do, it’ll probably be to the detriment of your choices.",
    },
    {
      author: "Derich4",
      date: "Fri Jul 05 2019 00:00:00 GMT+0800 (Philippine Standard Time)",
      keywords: ["Derich5", "test5"],
      speechMessage:
        "Speech 5 This is a group with many gifts. I’m sure one of your gifts is the gift of a smart and capable brain. I’m confident that’s the case because admission is competitive and if there weren’t some signs that you’re clever, the dean of admission wouldn’t have let you in.",
    },
  ];

  selectedSpeech: SpeechModel;

  create(speech) {
    console.log(speech);
    // this.speechesArray.push(speech);
  }

  update(speech) {
    if (speech) {
      let keywords = speech.keywords;
      let arrKeywords = keywords.split(",");
      speech.keywords = arrKeywords;
      speech.speechMessage = speech.speechMessage;

      this.speechesArray.push(speech);
      this.speeches.next(this.speechesArray);
      this.speechCache = null;
    }
  }

  delete(speechObject) {
    let index = this.speechesArray.lastIndexOf(speechObject);
    this.speechesArray.splice(index, 1);
    this.speeches.next(this.speechesArray);
  }

  filterSpeech(query: string) {
    this.filteredResult = query
      ? this.speechesArray.filter(
          (p) =>
            p.speechMessage.toLowerCase().includes(query.toLowerCase()) ||
            p.author.toLowerCase().includes(query.toLowerCase()) ||
            p.keywords.find(
              (word) => word.toLowerCase() === query.toLowerCase()
            )
        )
      : this.speechesArray;
  }

  filterAuthor(query: string) {
    this.filteredResult = query
      ? this.speechesArray.filter((p) =>
          p.author.toLowerCase().includes(query.toLowerCase())
        )
      : this.speechesArray;

    this.speeches.next(this.filteredResult);
  }

  filterKeywords(query: string) {
    this.filteredResult = query
      ? this.speechesArray.filter((p) => {
          return p.keywords.find((word) => word === query);
        })
      : this.speechesArray;
    this.speeches.next(this.filteredResult);
  }

  filterDate(query: string) {
    this.filteredResult = query
      ? this.speechesArray.filter((p) =>
          p.date.toLowerCase().includes(query.toLowerCase())
        )
      : this.speechesArray;
    this.speeches.next(this.filteredResult);
  }

  clearSearch() {
    this.filteredResult = [];
  }

  getSpeechList() {
    return this.speechesArray;
  }

  getFilteredResult() {
    return this.filteredResult;
  }

  setSelectedSpeech(obj) {
    this.selectedSpeech = obj;
  }

  getSelectedSpeech() {
    return of(this.selectedSpeech);
  }
}
