import { SpeechModel } from "./speech";

export interface SpeechViewConfig {
  isSearching: boolean;
}
