export interface SpeechModel {
  author: string;
  date: string;
  keywords: string[];
  speechMessage: string;
}
