export interface MainViewConfig {
  showSpeechList?: boolean;
  showControls?: boolean;
  showSearch?: boolean;
  enableDelete?: boolean;
  enableSave?: boolean;
  enableShare?: boolean;
  enableAuthor?: boolean;
  enableKeywords?: boolean;
  enableDate?: boolean;
}
