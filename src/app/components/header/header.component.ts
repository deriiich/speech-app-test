import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit {
  @Input("title") title: string;

  @Output("showList") showList = new EventEmitter<boolean>();

  isCollapsed: boolean;

  constructor() {}

  ngOnInit() {
    this.isCollapsed = true;
  }

  collapseNavbar() {
    this.isCollapsed = !this.isCollapsed;
  }

  closeNavbar() {
    this.isCollapsed = true;
  }

  showSpeechList() {
    this.showList.emit(true);
  }

  setTitle(title) {
    this.title = title;
  }
}
