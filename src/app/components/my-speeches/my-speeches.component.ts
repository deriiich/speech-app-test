import { Component, OnInit } from "@angular/core";
import { SpeechService } from "src/app/core/services/speech.service";

@Component({
  selector: "app-my-speeches",
  templateUrl: "./my-speeches.component.html",
  styleUrls: ["./my-speeches.component.scss"],
})
export class MySpeechesComponent implements OnInit {
  speechList = [];

  constructor(private speechService: SpeechService) {}

  ngOnInit() {
    this.speechList = this.speechService.getSpeechList();
  }
}
