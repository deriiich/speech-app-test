import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MySpeechesComponent } from './my-speeches.component';

describe('MySpeechesComponent', () => {
  let component: MySpeechesComponent;
  let fixture: ComponentFixture<MySpeechesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MySpeechesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MySpeechesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
