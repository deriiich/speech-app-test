import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BootstrapModule } from "src/app/bootstrap.module";
import { MySpeechesComponent } from "./my-speeches.component";

@NgModule({
  declarations: [MySpeechesComponent],
  exports: [MySpeechesComponent],
  imports: [CommonModule, BootstrapModule],
})
export class MySpeechesModule {}
