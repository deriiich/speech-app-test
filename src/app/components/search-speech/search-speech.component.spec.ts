import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SearchSpeechComponent } from './search-speech.component';

describe('SearchSpeechComponent', () => {
  let component: SearchSpeechComponent;
  let fixture: ComponentFixture<SearchSpeechComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchSpeechComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSpeechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
