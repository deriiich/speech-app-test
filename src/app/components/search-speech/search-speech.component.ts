import { Component, OnInit } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { map } from "rxjs/operators";
import { SpeechModel } from "src/app/core/models/speech";
import { SpeechService } from "src/app/core/services/speech.service";
@Component({
  selector: "app-search-speech",
  templateUrl: "./search-speech.component.html",
  styleUrls: ["./search-speech.component.scss"],
})
export class SearchSpeechComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
