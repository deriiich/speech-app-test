import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { BootstrapModule } from "src/app/bootstrap.module";
import { ControlsModule } from "../controls/controls.module";
import { SaveSpeechComponent } from "./save-speech.component";

@NgModule({
  declarations: [SaveSpeechComponent],
  exports: [SaveSpeechComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    BootstrapModule,
    FontAwesomeModule,
    ControlsModule,
  ],
})
export class SaveSpeechModule {}
