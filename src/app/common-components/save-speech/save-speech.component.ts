import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { SpeechModel } from "src/app/core/models/speech";
import { SpeechService } from "src/app/core/services/speech.service";

@Component({
  selector: "app-save-speech",
  templateUrl: "./save-speech.component.html",
  styleUrls: ["./save-speech.component.scss"],
})
export class SaveSpeechComponent implements OnInit {
  @ViewChild("speech") form: any;

  @Input() set selectedSpeechObj(obj: SpeechModel) {
    if (obj) {
      console.log(obj);
      this.speechObject = obj;
      this.speechMessage = this.speechObject.speechMessage;
    }
  }

  speechObject;
  speechMessage;
  isEmpty;

  temporarySpeech;

  constructor(private router: Router, private speechService: SpeechService) {}

  ngOnInit() {
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.speechObject = null;
        this.speechMessage = "";
        this.form.reset();
      }
    });
  }

  save(speech) {
    this.temporarySpeech = speech;
  }

  clearText() {
    this.temporarySpeech = "";
    this.speechMessage = "";
    this.form.reset();
  }
}
