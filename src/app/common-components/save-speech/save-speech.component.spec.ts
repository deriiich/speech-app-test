import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SaveSpeechComponent } from './save-speech.component';

describe('SaveSpeechComponent', () => {
  let component: SaveSpeechComponent;
  let fixture: ComponentFixture<SaveSpeechComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveSpeechComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveSpeechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
