import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SearchBarModule } from "./search-bar/search-bar.module";
import { ControlsModule } from "./controls/controls.module";
import { SaveSpeechModule } from "./save-speech/save-speech.module";
import { SpeechListModule } from "./speech-list/speech-list.module";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SearchBarModule,
    ControlsModule,
    SaveSpeechModule,
    SpeechListModule,
  ],
  exports: [
    CommonModule,
    SearchBarModule,
    ControlsModule,
    SaveSpeechModule,
    SpeechListModule,
  ],
})
export class SharedModule {}
