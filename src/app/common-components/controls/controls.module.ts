import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { BootstrapModule } from "src/app/bootstrap.module";
import { ControlsComponent } from "./controls.component";

@NgModule({
  declarations: [ControlsComponent],
  exports: [ControlsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    BootstrapModule,
    FontAwesomeModule,
  ],
})
export class ControlsModule {}
