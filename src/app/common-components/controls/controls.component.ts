import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from "@angular/core";
import {
  Form,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { NavigationEnd, Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { SpeechModel } from "src/app/core/models/speech";
import { SpeechService } from "src/app/core/services/speech.service";

@Component({
  selector: "app-controls",
  templateUrl: "./controls.component.html",
  styleUrls: ["./controls.component.scss"],
})
export class ControlsComponent implements OnInit {
  @Input("speechObject") set speechObjectConfig(obj: SpeechModel) {
    if (obj) {
      console.log(obj);
      this.speech = obj?.speechMessage;
      this.speechObject = obj;
    }
  }

  @Input("temporarySpeech") set temporarySpeech(speech: string) {
    if (speech) {
      this.tempSpeech = speech;
    }
  }

  @Output("save") saveEvent = new EventEmitter<boolean>();

  speech;
  speechObject;
  isReadOnly: boolean;
  speechForm: FormGroup;
  deleteDisable: boolean;
  saveDisable: boolean;
  shareDisable: boolean;

  isSubmitted;
  modalRef: BsModalRef;
  allowSearch;
  controlInputs: boolean;

  showDeleteButton: boolean = false;
  showSaveButton: boolean = true;
  showShareButton: boolean = true;

  tempSpeech: string = "";

  constructor(
    private speechService: SpeechService,
    private router: Router,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.speechForm = this.formBuilder.group({
      author: [""],
      keywords: [""],
      date: [""],
    });

    if (window.innerWidth >= 768) {
      this.showDeleteButton = true;
    } else {
      this.showDeleteButton = false;
    }

    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        this.isSubmitted = false;
        this.speechObject = null;
        this.speech = "";
        this.speechService.clearSearch();

        switch (e.url) {
          case "/my-speeches":
            {
              this.isReadOnly = true;
              this.deleteDisable = false;
              this.saveDisable = true;
              this.shareDisable = false;

              this.speechForm = this.formBuilder.group({
                author: [""],
                keywords: [""],
                date: [""],
              });
            }
            break;
          case "/create-speech":
            {
              this.isReadOnly = false;
              this.deleteDisable = true;
              this.saveDisable = false;
              this.shareDisable = true;
              this.allowSearch = false;
              this.showSaveButton = true;

              this.speechForm = this.formBuilder.group({
                author: ["", Validators.required],
                keywords: ["", Validators.required],
                date: ["", Validators.required],
              });
            }
            break;
          case "/search-speech":
            {
              this.isReadOnly = false;
              this.deleteDisable = true;
              this.saveDisable = true;
              this.shareDisable = true;
              this.allowSearch = true;
              this.speechForm = this.formBuilder.group({
                author: ["", Validators.required],
                keywords: ["", Validators.required],
                date: ["", Validators.required],
              });
            }
            break;
        }
      }
    });
  }

  get f() {
    return this.speechForm.controls;
  }

  delete() {
    if (this.speechObject) {
      this.speechService.delete(this.speechObject);
      this.speechObject = null;
    }
  }

  save() {
    this.isSubmitted = true;
    if (this.speechForm.valid && this.tempSpeech) {
      this.speechForm.addControl(
        "speechMessage",
        new FormControl(this.tempSpeech, Validators.required)
      );
      console.log(this.speechForm.value);
      this.speechService.update(this.speechForm.value);
      this.isSubmitted = false;
      this.speechForm.reset();
      this.saveEvent.emit();
      this.router.navigate(["/my-speeches"]);
    }
  }

  share(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  chooseSocial(social) {
    this.modalRef.hide();
    alert("Sharing to " + social);
  }

  search(key, value) {
    if (this.allowSearch) {
      switch (key) {
        case "author":
          {
            if (value) {
              this.speechService.filterAuthor(value);
              // this.speechService.setShowList(true);
            } else {
              // this.speechService.setShowList(false);
            }
          }
          break;
        case "keywords":
          {
            if (value) {
              this.speechService.filterKeywords(value);
              // this.speechService.setShowList(true);
            } else {
              // this.speechService.setShowList(false);
            }
          }
          break;
        case "date":
          {
            if (value) {
              this.speechService.filterDate(value);
              // this.speechService.setShowList(true);
            } else {
              // this.speechService.setShowList(false);
            }
          }
          break;
      }
    }
  }

  @HostListener("window:resize", [])
  onResize() {
    if (window.innerWidth >= 768) {
      this.showDeleteButton = true;
    } else {
      this.showDeleteButton = false;
    }
  }
}
