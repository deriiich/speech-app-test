import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { SpeechViewConfig } from "src/app/core/models/speechView.config";

import { SpeechService } from "../../core/services/speech.service";

@Component({
  selector: "app-speech-list",
  templateUrl: "./speech-list.component.html",
  styleUrls: ["./speech-list.component.scss"],
})
export class SpeechListComponent implements OnInit {
  @Input("speechViewConfig") set speechViewConfig(config: SpeechViewConfig) {
    console.log(config);
    this.setArray(config);
  }

  @Output("select") select = new EventEmitter();

  isSearching;

  speechListArray = [];

  constructor(private speechService: SpeechService) {}

  ngOnInit() {
    this.speechListArray = this.speechService.getSpeechList();
  }
  setArray(config: SpeechViewConfig) {
    if (config.isSearching) {
      this.speechListArray = this.speechService.getFilteredResult();
      this.isSearching = true;
    } else {
      this.speechListArray = this.speechService.getSpeechList();
      this.isSearching = false;
    }
  }

  selectSpeech(speechObject) {
    this.select.emit(speechObject);
    // this.speechService.setSelectedSpeech(speechObject);
  }
}
