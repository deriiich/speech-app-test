import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SpeechListComponent } from "./speech-list.component";

@NgModule({
  declarations: [SpeechListComponent],
  imports: [CommonModule],
  exports: [SpeechListComponent],
})
export class SpeechListModule {}
