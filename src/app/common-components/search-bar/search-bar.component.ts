import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { SpeechModel } from "src/app/core/models/speech";
import { map } from "rxjs/operators";
import { SpeechService } from "src/app/core/services/speech.service";
@Component({
  selector: "app-search-bar",
  templateUrl: "./search-bar.component.html",
  styleUrls: ["./search-bar.component.scss"],
})
export class SearchBarComponent implements OnInit, OnDestroy {
  @Output("searchSpeechOutput") searchSpeechOutput = new EventEmitter<string>();
  @Input("isVisible") isVisible = true;

  searchKeyword: string;
  searchedSpeech: SpeechModel[];
  selectedSpeech: SpeechModel;
  modalRef?: BsModalRef;

  constructor(
    private speechService: SpeechService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {}

  ngOnDestroy() {
    this.searchSpeechOutput.emit("");
  }

  search(search: string) {
    if (search.length > 0) {
      this.searchKeyword = search;
      this.searchSpeechOutput.emit(this.searchKeyword);
      this.speechService.filterSpeech(this.searchKeyword);
      this.searchedSpeech = this.speechService.getFilteredResult();
    } else {
      this.searchSpeechOutput.emit("");
      this.speechService.clearSearch();
      this.searchedSpeech = this.speechService.getSpeechList();
    }
  }

  clear(input: HTMLInputElement) {
    input.value = "";
    this.speechService.clearSearch();
    this.searchSpeechOutput.emit("");
  }

  selectList(selectedSpeech, template) {
    this.selectedSpeech = selectedSpeech;
    this.modalRef = this.modalService.show(template);
  }

  confirmDelete(speechObject) {
    let alert = confirm("Are you sure you want to delete?");
    if (alert) {
      this.speechService.delete(speechObject);
      this.modalService.hide();
      this.search(this.searchKeyword);
    }
  }

  @HostListener("window:resize")
  onresize() {
    this.searchedSpeech = [];
  }
}
