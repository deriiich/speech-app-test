import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SearchBarComponent } from "./search-bar.component";
import { BootstrapModule } from "src/app/bootstrap.module";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";

@NgModule({
  declarations: [SearchBarComponent],
  imports: [CommonModule, BootstrapModule, FontAwesomeModule],
  exports: [SearchBarComponent],
})
export class SearchBarModule {}
