// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config: {
    apiKey: "AIzaSyA4EEOAHgu_-PF_AO0E5pX-674J-8Rl08Y",
    authDomain: "speech-app-bb149.firebaseapp.com",
    projectId: "speech-app-bb149",
    storageBucket: "speech-app-bb149.appspot.com",
    messagingSenderId: "562787307142",
    appId: "1:562787307142:web:39e4f40b202f63ccee8b07",
    measurementId: "G-SF9YNYYCC8",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
